﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herramienta_Administrativa_VerifID.Clases
{
    class Depositos
    {
        public int IdDeposito { get; set; }

        public string Nombre { get; set; }

        public string Codigo { get; set; }

        public override string ToString()
        {
            return Nombre;
        }

    }
}
