﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Herramienta_Administrativa_VerifID.Clases
{
    class Registro
    {

        public int IdRegistro { get; set; }
        public string URLFoto { get; set; }
        public string URLFotoCompacta { get; set; }
    }
}
