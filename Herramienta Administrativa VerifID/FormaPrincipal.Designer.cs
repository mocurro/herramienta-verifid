﻿namespace Herramienta_Administrativa_VerifID
{
    partial class FormaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FechaInicio = new DevExpress.XtraEditors.DateEdit();
            this.FechaFin = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnConvertir = new DevExpress.XtraEditors.SimpleButton();
            this.progressBarUpdate = new DevExpress.XtraEditors.ProgressBarControl();
            this.progressBar = new DevExpress.XtraWaitForm.ProgressPanel();
            this.Obtener = new DevExpress.XtraEditors.SimpleButton();
            this.ListadoMensajes = new System.Windows.Forms.RichTextBox();
            this.backgroundWorkerBusqueda = new System.ComponentModel.BackgroundWorker();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.comboDepositos = new DevExpress.XtraEditors.ComboBoxEdit();
            this.backgroundWorkerUpdate = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFin.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarUpdate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboDepositos.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // FechaInicio
            // 
            this.FechaInicio.EditValue = null;
            this.FechaInicio.Location = new System.Drawing.Point(372, 63);
            this.FechaInicio.Name = "FechaInicio";
            this.FechaInicio.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.FechaInicio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaInicio.Properties.Appearance.Options.UseFont = true;
            this.FechaInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FechaInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FechaInicio.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.FechaInicio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FechaInicio.Size = new System.Drawing.Size(157, 30);
            this.FechaInicio.TabIndex = 1;
            // 
            // FechaFin
            // 
            this.FechaFin.EditValue = null;
            this.FechaFin.Location = new System.Drawing.Point(639, 63);
            this.FechaFin.Name = "FechaFin";
            this.FechaFin.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FechaFin.Properties.Appearance.Options.UseFont = true;
            this.FechaFin.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FechaFin.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FechaFin.Properties.DisplayFormat.FormatString = "yyyy-MM-dd";
            this.FechaFin.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.FechaFin.Size = new System.Drawing.Size(157, 30);
            this.FechaFin.TabIndex = 2;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl1.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(256, 66);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(110, 25);
            this.labelControl1.TabIndex = 3;
            this.labelControl1.Text = "Fecha inicio";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl2.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(547, 64);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(86, 25);
            this.labelControl2.TabIndex = 4;
            this.labelControl2.Text = "Fecha fin";
            // 
            // btnConvertir
            // 
            this.btnConvertir.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConvertir.Appearance.Options.UseFont = true;
            this.btnConvertir.Location = new System.Drawing.Point(375, 386);
            this.btnConvertir.Name = "btnConvertir";
            this.btnConvertir.Size = new System.Drawing.Size(227, 74);
            this.btnConvertir.TabIndex = 5;
            this.btnConvertir.Text = "Convertir imagen";
            this.btnConvertir.Visible = false;
            this.btnConvertir.Click += new System.EventHandler(this.btnConvertir_Click);
            // 
            // progressBarUpdate
            // 
            this.progressBarUpdate.Location = new System.Drawing.Point(51, 484);
            this.progressBarUpdate.Name = "progressBarUpdate";
            this.progressBarUpdate.Properties.ShowTitle = true;
            this.progressBarUpdate.ShowProgressInTaskBar = true;
            this.progressBarUpdate.Size = new System.Drawing.Size(851, 47);
            this.progressBarUpdate.TabIndex = 6;
            this.progressBarUpdate.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.progressBar.Appearance.Options.UseBackColor = true;
            this.progressBar.BarAnimationElementThickness = 2;
            this.progressBar.Caption = "Espere un momento por favor";
            this.progressBar.Description = "Obteniendo información..";
            this.progressBar.Location = new System.Drawing.Point(314, 284);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(291, 96);
            this.progressBar.TabIndex = 7;
            this.progressBar.Text = "Espere un momento por favor";
            this.progressBar.Visible = false;
            // 
            // Obtener
            // 
            this.Obtener.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Obtener.Appearance.Options.UseFont = true;
            this.Obtener.Location = new System.Drawing.Point(827, 48);
            this.Obtener.Name = "Obtener";
            this.Obtener.Size = new System.Drawing.Size(148, 55);
            this.Obtener.TabIndex = 8;
            this.Obtener.Text = "Obtener";
            this.Obtener.Click += new System.EventHandler(this.Obtener_Click);
            // 
            // ListadoMensajes
            // 
            this.ListadoMensajes.Location = new System.Drawing.Point(32, 125);
            this.ListadoMensajes.Name = "ListadoMensajes";
            this.ListadoMensajes.Size = new System.Drawing.Size(943, 153);
            this.ListadoMensajes.TabIndex = 9;
            this.ListadoMensajes.Text = "";
            // 
            // backgroundWorkerBusqueda
            // 
            this.backgroundWorkerBusqueda.WorkerReportsProgress = true;
            this.backgroundWorkerBusqueda.WorkerSupportsCancellation = true;
            this.backgroundWorkerBusqueda.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerBusqueda_DoWork);
            this.backgroundWorkerBusqueda.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerBusqueda_RunWorkerCompleted);
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelControl3.Appearance.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            this.labelControl3.Location = new System.Drawing.Point(12, 64);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(89, 25);
            this.labelControl3.TabIndex = 10;
            this.labelControl3.Text = "Depositos";
            // 
            // comboDepositos
            // 
            this.comboDepositos.Location = new System.Drawing.Point(107, 73);
            this.comboDepositos.Name = "comboDepositos";
            this.comboDepositos.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboDepositos.Properties.Appearance.Options.UseFont = true;
            this.comboDepositos.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboDepositos.Size = new System.Drawing.Size(124, 26);
            this.comboDepositos.TabIndex = 12;
            this.comboDepositos.SelectedValueChanged += new System.EventHandler(this.comboDepositos_SelectedValueChanged);
            // 
            // backgroundWorkerUpdate
            // 
            this.backgroundWorkerUpdate.WorkerReportsProgress = true;
            this.backgroundWorkerUpdate.WorkerSupportsCancellation = true;
            this.backgroundWorkerUpdate.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerUpdate_DoWork);
            this.backgroundWorkerUpdate.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorkerUpdate_ProgressChanged);
            this.backgroundWorkerUpdate.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerUpdate_RunWorkerCompleted);
            // 
            // FormaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 543);
            this.Controls.Add(this.comboDepositos);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.ListadoMensajes);
            this.Controls.Add(this.Obtener);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.progressBarUpdate);
            this.Controls.Add(this.btnConvertir);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.FechaFin);
            this.Controls.Add(this.FechaInicio);
            this.Name = "FormaPrincipal";
            this.Text = "Herramienta Administrativa";
            this.Load += new System.EventHandler(this.FormaPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFin.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaFin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarUpdate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboDepositos.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.DateEdit FechaInicio;
        private DevExpress.XtraEditors.DateEdit FechaFin;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnConvertir;
        private DevExpress.XtraEditors.ProgressBarControl progressBarUpdate;
        private DevExpress.XtraWaitForm.ProgressPanel progressBar;
        private DevExpress.XtraEditors.SimpleButton Obtener;
        private System.Windows.Forms.RichTextBox ListadoMensajes;
        private System.ComponentModel.BackgroundWorker backgroundWorkerBusqueda;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.ComboBoxEdit comboDepositos;
        protected System.ComponentModel.BackgroundWorker backgroundWorkerUpdate;
    }
}

