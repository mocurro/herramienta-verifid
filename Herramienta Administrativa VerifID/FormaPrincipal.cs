﻿using DevExpress.XtraEditors;
using Herramienta_Administrativa_VerifID.Clases;
using ImageMagick;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;

namespace Herramienta_Administrativa_VerifID
{
    public partial class FormaPrincipal : Form

    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        // String strconexion = ConfigurationManager.ConnectionStrings["CocexionTI"].ConnectionString;
        string connectionStringProducción = "Data Source=pvcf7e82l1.database.windows.net; Initial Catalog=Reparto-DanonePruebas; Trusted_Connection=False; User ID=danone;Password=wqEYZ4c1Zm2oVYE7";
        string connectionStringDesarrollo = "Data Source=177.231.60.43\\dev; Initial Catalog=Manhattan; Trusted_Connection=False; User ID=sa;Password=VerifID2";
        string MensajeError = "";
        
        

        List<Registro> ListaRegistro= new List<Registro>() ;

        List<Depositos > ListaDepositos = new List<Depositos>();

        Depositos miDepositoSeleccionado;


        public FormaPrincipal()
        {
            InitializeComponent();
        }

        private void ObtenerRegistros() {
            ListaRegistro.Clear();

            using (SqlConnection cnx = new SqlConnection(connectionStringDesarrollo))
            {
                cnx.Open();
                using (SqlCommand objCmd = new SqlCommand())
                {
                    objCmd.Connection = cnx;

                    objCmd.CommandText = Properties.Settings.Default.GetRegistro;//string.Format("Select IdTag,MiTag from Tags WHERE MiTag= '{0}' and MiTag<>'';", GuidTag);

                    SqlDataReader registros = objCmd.ExecuteReader();
                    while (registros.Read())
                    {
                        Registro miRegistro = new Registro();
                        miRegistro.IdRegistro  = int.Parse(registros["IdRegistro"].ToString());

                        miRegistro.URLFoto  = (registros["UrlFoto"].ToString());


                        ListaRegistro.Add(miRegistro);
                    }
                    registros.Close();
                }
                cnx.Close();
            }

            if (ListaRegistro.Count > 0) {
                using (SqlConnection cnx = new SqlConnection(connectionStringDesarrollo))
                {
                    cnx.Open();
                    foreach (Registro element in ListaRegistro)
                    {
                        Encoding unicode = Encoding.Unicode;

                        //byte[] imageBytes = Convert.FromBase64String(base64String);
                        element.URLFotoCompacta = CompactarImagen(Convert.FromBase64String(element.URLFoto));
                        using (SqlCommand objCmd = new SqlCommand())
                        {

                            objCmd.Connection = cnx;

                            objCmd.CommandText = string.Format("INSERT INTO Registros(IdRegistro,UrlFoto,UrlFotoCompacta) VALUES({0},'{1}','{2}')", element.IdRegistro , element.URLFoto , element.URLFotoCompacta );

                            objCmd.ExecuteNonQuery();
                        }
                    }                        
                    cnx.Close();

                }
            }           

        }

        private string CompactarImagen(byte[] bytemagen)
        {
            using (var image = new MagickImage(bytemagen))
            {
                image.Resize(600,0);
                //Mayor calidad 100 menor calidad 0
                image.Quality = 15;
                return image.ToBase64();
            }
        }                
               

        public string ImageToBase64(Image image)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms,ImageFormat.Jpeg );
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to base 64 string
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        private void btnConvertir_Click(object sender, EventArgs e)
        {
            btnConvertir.Enabled = false;
            progressBar.Visible = true;
            progressBar.Description = "Actualizando información..";
            MensajeError = "";
            if (backgroundWorkerUpdate .IsBusy != true)
            {
                // Start the asynchronous operation.
                //backgroundWorkerUpdate.ReportProgress();
                backgroundWorkerUpdate.RunWorkerAsync();
            }
        }

        private void Obtener_Click(object sender, EventArgs e)
        {
            //ObtenerRegistros();
            Obtener.Enabled = false;
            log.Info("Obteniendo información.");
            progressBar.Description = "Obteniendo información..";
            progressBar.Visible = true;
            if (backgroundWorkerBusqueda.IsBusy != true)
            {
                // Start the asynchronous operation.
                backgroundWorkerBusqueda.RunWorkerAsync();
            }

        }

        private void backgroundWorkerBusqueda_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ListaRegistro.Clear();

                using (SqlConnection cnx = new SqlConnection(connectionStringProducción))
                {
                    cnx.Open();
                    using (SqlCommand objCmd = new SqlCommand())
                    {
                        objCmd.Connection = cnx;
                        var comando= Properties.Settings.Default.GetRegistroFecha;
                        comando = comando.Replace( "{FechaInicio}", FechaInicio.Text );
                        comando = comando.Replace("{FechaFin}", FechaFin.Text);
                        comando = comando.Replace("{IdDeposito}", miDepositoSeleccionado .IdDeposito.ToString () );
                        objCmd.CommandText = comando;

                        SqlDataReader registros = objCmd.ExecuteReader();
                        while (registros.Read())
                        {
                            Registro miRegistro = new Registro();
                            miRegistro.IdRegistro = int.Parse(registros["IdRegistro"].ToString());

                            //miRegistro.URLFoto = (registros["UrlFoto"].ToString());


                            ListaRegistro.Add(miRegistro);
                        }
                        registros.Close();
                    }
                    cnx.Close();
                }
                log.Info("Se obtuvieron:"+ ListaRegistro.Count );
               

            }
            catch (Exception ex) {
                MensajeError = ex.Message;
                log.Error ("backgroundWorkerBusqueda_DoWork"+ex.Message);
            }
        }

        private void backgroundWorkerBusqueda_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressBar.Visible = false;

            progressBarUpdate.Properties.Maximum = ListaRegistro.Count;
            if (MensajeError.Equals(""))
            {
                ListadoMensajes.AppendText("La cantidad de registros por atualizar es:" + ListaRegistro.Count+ Environment.NewLine);
                Obtener.Enabled = true;
                btnConvertir.Visible = true;
            }
            else {
                ListadoMensajes.AppendText("Error al obtener la información:" + MensajeError + Environment.NewLine);
                Obtener.Enabled = true;
            }
            
        }

        private void FormaPrincipal_Load(object sender, EventArgs e)
        {
            FechaInicio.DateTime = DateTime.Now;
            FechaFin.DateTime = DateTime.Now;
            log.Debug("Inicio de aplicación");
            ObtenerDepositos();

        }

        private void ObtenerDepositos() {
            try
            {
                ListaDepositos.Clear();

                using (SqlConnection cnx = new SqlConnection(connectionStringProducción))
                {
                    cnx.Open();
                    using (SqlCommand objCmd = new SqlCommand())
                    {
                        objCmd.Connection = cnx;
                        var comando = Properties.Settings.Default.GetDepositos ;
                        objCmd.CommandText = comando;

                        SqlDataReader registros = objCmd.ExecuteReader();
                        while (registros.Read())
                        {
                            Depositos  miDeposito = new Depositos();
                            miDeposito.IdDeposito  = int.Parse(registros["IdDeposito"].ToString());
                            miDeposito.Nombre = (registros["Nombre"].ToString());

                            ListaDepositos .Add(miDeposito);
                        }
                        registros.Close();
                    }
                    cnx.Close();
                }
                foreach (Depositos  element in ListaDepositos)
                {
                    comboDepositos.Properties.Items.Add(element);
                }
                comboDepositos.SelectedIndex = 0;


                }
            catch (Exception ex)
            {
                MensajeError = ex.Message;
                log.Error("ObtenerDepositos" + ex.Message);
            }
        }

        private void comboDepositos_SelectedValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit comboBox = sender as ComboBoxEdit;
            miDepositoSeleccionado = (Depositos) comboBox.SelectedItem;
        }

        private void backgroundWorkerUpdate_DoWork(object sender, DoWorkEventArgs e)
        {
            try {
                int noRegistro = 0;
                if (ListaRegistro.Count > 0)
                {
                    foreach (Registro element in ListaRegistro)
                    {
                        noRegistro += 1;
                        using (SqlConnection cnx = new SqlConnection(connectionStringProducción ))
                        {
                            cnx.Open();
                            using (SqlCommand objCmd = new SqlCommand())
                            {
                                objCmd.Connection = cnx;


                                var comando = Properties.Settings.Default.GetRegistroFotoXID;
                                comando = comando.Replace("{idregistro}", element.IdRegistro.ToString () );
                                objCmd.CommandText = comando;
                                SqlDataReader registros = objCmd.ExecuteReader();
                                while (registros.Read())
                                {
                                    element.URLFoto = (registros["UrlFoto"].ToString());
                                    element.URLFotoCompacta = CompactarImagen(Convert.FromBase64String(element.URLFoto));
                                    log.Info("Se Modificó el registro:"+ element.IdRegistro.ToString()  +" URLFoto:" + element.URLFoto.Count () +" URLFotoCompacta:" + element.URLFotoCompacta .Count());
                                    break;
                                }
                                registros.Close();
                            }
                            cnx.Close();
                        }

                        


                        //using (SqlConnection cnx = new SqlConnection(connectionStringDesarrollo))
                        //{
                        //    cnx.Open();
                                                        
                        //        using (SqlCommand objCmd = new SqlCommand())
                        //        {

                        //            objCmd.Connection = cnx;

                        //            objCmd.CommandText = string.Format("INSERT INTO Registros(IdRegistro,UrlFoto,UrlFotoCompacta) VALUES({0},'{1}','{2}')", element.IdRegistro, element.URLFoto, element.URLFotoCompacta);

                        //            objCmd.ExecuteNonQuery();
                        //           // backgroundWorkerUpdate .ReportProgress(noRegistro);
                        //    }                            
                        //    cnx.Close();
                        //}

                        using (SqlConnection cnx = new SqlConnection(connectionStringProducción))
                        {
                            cnx.Open();

                            using (SqlCommand objCmd = new SqlCommand())
                            {

                                objCmd.Connection = cnx;
                                objCmd.CommandText = string.Format("Update Registros set UrlFoto= '{0}' where idregistro={1}", element.URLFotoCompacta, element.IdRegistro);

                                objCmd.ExecuteNonQuery();
                                backgroundWorkerUpdate.ReportProgress(noRegistro);
                            }
                            cnx.Close();
                        }
                    }
                }
            } catch (Exception ex) {
                MensajeError = "Error al obtener la información:"+ ex.Message;
            }
            
        }

        private void backgroundWorkerUpdate_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnConvertir.Enabled = false;
            progressBar.Visible = false;
            progressBarUpdate.Visible = false;

            if (MensajeError.Equals(""))
            {
                ListadoMensajes.AppendText("Se actualizaron correctamente los registros"+ Environment.NewLine);
            }
            else {
                ListadoMensajes.AppendText("Error al actualizar la información:" + MensajeError + Environment.NewLine);
            }
        }

        private void backgroundWorkerUpdate_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarUpdate.Visible = true;

            progressBarUpdate.Position = e.ProgressPercentage;
        }
    }
}
